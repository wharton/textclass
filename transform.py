# First we do the necessary imports
import pandas as pd
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.model_selection import train_test_split
import pickle

# Then we read our data in from a file
df = pd.read_csv("amz_reviews_tokenized.csv.gz",compression="gzip")

# Now we define a little function that just takes a space-delimited string of tokens and turns it into a list
def to_tokens(tokstr):
    return tokstr.split()

# Then we create the object that will carry out the TF-IDF algorithm on the input data.
vectorizer = TfidfVectorizer(tokenizer = to_tokens)

# Now we use the object to actually transform the strings of tokens into fixed-dimension vectors
features = vectorizer.fit_transform(df['tokens'])

# Save the vectorizer to a file for use in future inference
with open("vectorizer","wb") as outfile:
    pickle.dump(vectorizer,outfile)

# We split the data in order to hold out 20% and make sure we have some data for testing that we haven't used in training
X_train, X_test, y_train, y_test = train_test_split(features,df['label'], test_size=0.2)

# Finally, we save the transformed input data to files
with open("X_train","wb") as outfile:
    pickle.dump(X_train,outfile)

with open("y_train","wb") as outfile:
    pickle.dump(y_train,outfile)

with open("X_test","wb") as outfile:
    pickle.dump(X_test,outfile)

with open("y_test","wb") as outfile:
    pickle.dump(y_test,outfile)

