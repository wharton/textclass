# First we do the necessary imports
import pandas as pd
from gensim.models import Doc2Vec
from gensim.models.doc2vec import TaggedDocument
from sklearn.model_selection import train_test_split
import pickle
import os

# Determine the number of cores available to us
try:
    cores = int(os.environ.get('OMP_NUM_THREADS'))
except:
    cores = 1

# Then we read our data in from a file
df = pd.read_csv("amz_reviews_tokenized.csv.gz",compression="gzip")

# Now we define a little function that just takes a space-delimited string of tokens and turns it into a list
def to_tokens(tokstr):
    return tokstr.split()

# Then we create the object that will carry out the doc2vec algorithm on the input data.
# We will run it on all cores available
doc_model = Doc2Vec(dm=0, vector_size=300, negative=5, hs=0, min_count=2, sample = 0, workers=cores)

# This is a little function that we'll use to convert space separated strings into TaggedDocument
# objects, which are what doc2vec wants as input
def create_doc(row):
    doc = TaggedDocument(words=row['tokens'].split(),tags=[row.name])
    return doc

# Use create_doc to make a Series of TaggedDocuments
train_docs = df.apply(create_doc , axis=1)

# Build a vocabulary from the input documents
doc_model.build_vocab(train_docs.values)

# Train the document model on the input documents
doc_model.train(train_docs.values, total_examples=len(train_docs.values), epochs=30)

# Save the vectorizer (document model) to a file for use in future inference
with open("vectorizer","wb") as outfile:
    pickle.dump(doc_model,outfile)


# Now we use the document model to actually transform the strings of tokens into fixed-dimension vectors
features = [doc_model.infer_vector(doc.words, steps=20) for doc in train_docs]

# We split the data in order to hold out 20% and make sure we have some data for testing that we haven't used in training
X_train, X_test, y_train, y_test = train_test_split(features,df['label'], test_size=0.2)

# Finally, we save the transformed input data to files
with open("X_train","wb") as outfile:
    pickle.dump(X_train,outfile)

with open("y_train","wb") as outfile:
    pickle.dump(y_train,outfile)

with open("X_test","wb") as outfile:
    pickle.dump(X_test,outfile)

with open("y_test","wb") as outfile:
    pickle.dump(y_test,outfile)

