#$ -j y

# enable python3
source /opt/rh/rh-python36/enable

# enter virtual environment
source env/bin/activate

# run the python script passed in as the first option
python $1
