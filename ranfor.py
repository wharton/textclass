# First we do the necessary imports
import pickle
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import accuracy_score,f1_score,roc_auc_score
from os import path, rename
import numpy as np
import os

# Determine the number of cores that are available
try:
    cores = int(os.environ.get('OMP_NUM_THREADS'))
except:
    cores = 1

# Now we load all the training and test data in from files
with open("X_train","rb") as infile:
    X_train = pickle.load(infile)

with open("X_test","rb") as infile:
    X_test = pickle.load(infile)

with open("y_train","rb") as infile:
    y_train = pickle.load(infile)

with open("y_test","rb") as infile:
    y_test = pickle.load(infile)

# Then using a cross-validation approach, we split our training batch for this iteration into training and validation sets
X_train, X_val, y_train, y_val = train_test_split(X_train, y_train, test_size=0.2)

# If this is the first time we are training, a file wont exist for our model. So we need to check.
if path.exists("ranfor_model"):
    # If a file exists for the model
    #read the model in from the file
    with open("ranfor_model","rb") as infile:
        model = pickle.load(infile)
    # Save a version of the previous in case this one did not perform as well
    rename('ranfor_model','ranfor_model_previous')
    rename('ranfor_results','ranfor_results_previous')
else:
    # If no file exists for the model, create a new model
    # We'll use all 8 cores to speed things up. Make sure to specify -pe openmp 8 when running qsub
    model = RandomForestClassifier(n_jobs=8)

# Train the model
model.fit(X_train,y_train)

# Get predicted labels for the validation set from the trained model
preds = model.predict(X_val)

# Open file to write output
with open("ranfor_results","w") as outfile:

    # Print some performance metrics using the predicted labels and the actual labels for the validation set
    outfile.write("validation accuracy: "+str(accuracy_score(preds,y_val))+"\n")
    outfile.write("validation f1_score: "+str(f1_score(preds,y_val,average='weighted'))+"\n")

    # Get label probabilities for the validation set from the trained model
    probs = model.predict_proba(X_val)

    # Print performance metrics which are based on label probabilities
    outfile.write("validation roc_auc: "+str(roc_auc_score(y_val, probs[:,1].T))+"\n")

    # Repeat the metrics calculations on the holdout test set 
    preds = model.predict(X_test)
    outfile.write("holdout accuracy: "+str(accuracy_score(preds,y_test))+"\n")
    outfile.write("holdout f1_score: "+str(f1_score(preds,y_test,average='weighted'))+"\n")
    probs = model.predict_proba(X_test)
    outfile.write("holdout roc_auc: "+str(roc_auc_score(y_test, probs[:,1].T))+"\n")


# Save the trained model for the next iteration
with open("ranfor_model","wb") as outfile:
    pickle.dump(model, outfile)
