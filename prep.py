# first do the necessary imports
import string
import pandas as pd
import nltk
from nltk.corpus import stopwords
from nltk.stem.snowball import SnowballStemmer

# then read in raw data
df = pd.read_csv("amz_reviews_labeled.csv.gz",compression="gzip")

# convert the reviews to lowercase
df['review'] = df['review'].str.lower()

# remove punctuation from the reviews
df['review'] = df['review'].str.translate(str.maketrans('', '', string.punctuation))

# download a set of overly common words ("stopwords") to remove from the input
nltk.download('stopwords')

# cache the stopwords for english to improve performance
cachedStopWords = stopwords.words("english")

# now create a stemmer (which will remove syntax-related endings such as "-ing" that are "noise" to tf-idf)
stemmer = SnowballStemmer("english")

# then we define a tokenization function
# the function takes a single row of the data, removes the stopwords, stems the remaining words, and
# returns the result as a space-separated string
def tokenize(row):
    words = row['review'].split()
    stems = []
    for word in words:
        if word not in cachedStopWords:
            stems.append(stemmer.stem(word))
    return ' '.join(stems)

# now let's use the function to create a new column in the dataframe by appling the tokenize 
# function to the reviews
df['tokens'] = df.apply(tokenize,axis=1)

# finally save the dataframe with the new column as a csv
df.to_csv("amz_reviews_tokenized.csv.gz",index=False,compression="gzip")

