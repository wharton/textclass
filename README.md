# README #

### What is this repository for? ###

* This repo is an example of a text classification project to serve as scaffolding for those who want to run their own text classification jobs in Python on the HPCC

### How do I get set up? ###

* Detailed instructions are at [https://research-it.wharton.upenn.edu/tools/text-classification-with-python-on-the-hpcc](https://research-it.wharton.upenn.edu/news/text-classification-with-python-on-the-hpcc)

### Who do I talk to? ###

* research-programming@wharton.upenn.edu
